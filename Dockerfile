FROM golang:1.16-alpine

RUN apk add --no-cache git

WORKDIR /app/voting-system-blockchain

COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY . .

RUN go build -o /out/voting-system-blockchain .

EXPOSE 8080

CMD ["./out/voting-system-blockchain"] 
