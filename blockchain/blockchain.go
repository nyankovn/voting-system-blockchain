package blockchain

import (
	"fmt"

	"github.com/dgraph-io/badger"
	"gitlab.com/nyankovn/voting-system-blockchain/util"
)

const dbPath = "./tmp/blocks"

type BlockChain struct {
	LastHash []byte
	Database *badger.DB
}

type BlockChainIterator struct {
	CurrentHash []byte
	Database    *badger.DB
}

func InitBlockChain() (*BlockChain, error) {
	var lastHash []byte

	opts := badger.DefaultOptions(dbPath)

	db, err := badger.Open(opts)
	if err != nil {
		return nil, err
	}

	err = db.Update(func(txn *badger.Txn) error {
		// "lh" stand for last hash
		if _, err := txn.Get([]byte("lh")); err == badger.ErrKeyNotFound {
			fmt.Println("No existing blockchain found")
			genesis := Genesis()
			genesisBytes, err := genesis.Serialize()
			if err != nil {
				return err
			}

			fmt.Println("Genesis proved")
			err = txn.Set(genesis.Hash, genesisBytes)
			util.HandleFatal(err)
			err = txn.Set([]byte("lh"), genesis.Hash)

			lastHash = genesis.Hash

			return err
		} else {
			item, err := txn.Get([]byte("lh"))
			util.HandleFatal(err)
			err = item.Value(func(val []byte) error {
				lastHash = val
				return nil
			})
			util.HandleFatal(err)
			return err
		}
	})

	if err != nil {
		return nil, err
	}

	blockchain := BlockChain{lastHash, db}
	return &blockchain, nil
}

func (chain *BlockChain) AddBlock(data string) {
	var lastHash []byte

	err := chain.Database.View(func(txn *badger.Txn) error {
		item, err := txn.Get([]byte("lh"))
		util.HandleFatal(err)
		err = item.Value(func(val []byte) error {
			lastHash = val
			return nil
		})
		util.HandleFatal(err)
		return err
	})
	util.HandleFatal(err)

	newBlock := CreateBlock(data, lastHash)
	blockBytes, err := newBlock.Serialize()
	util.HandleFatal(err)

	err = chain.Database.Update(func(transaction *badger.Txn) error {
		err := transaction.Set(newBlock.Hash, blockBytes)
		util.HandleFatal(err)
		err = transaction.Set([]byte("lh"), newBlock.Hash)

		chain.LastHash = newBlock.Hash
		return err
	})
	util.HandleFatal(err)
}

func (chain *BlockChain) Iterator() *BlockChainIterator {
	iterator := BlockChainIterator{chain.LastHash, chain.Database}

	return &iterator
}

func (iterator *BlockChainIterator) Next() *Block {
	var block *Block

	err := iterator.Database.View(func(txn *badger.Txn) error {
		item, err := txn.Get(iterator.CurrentHash)
		util.HandleFatal(err)

		err = item.Value(func(val []byte) error {

			block, err = Deserialize(val)
			if err != nil {
				return err
			}
			return nil
		})
		util.HandleFatal(err)
		return err
	})
	util.HandleFatal(err)

	iterator.CurrentHash = block.PrevHash

	return block
}
