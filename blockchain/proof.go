package blockchain

import (
	"bytes"
	"crypto/sha256"
	"encoding/binary"
	"fmt"
	"math"
	"math/big"

	"gitlab.com/nyankovn/voting-system-blockchain/util"
)

const Difficulty = 12

type ProofOfWork struct {
	Block  *Block
	Target *big.Int
}

func NewProofOfWork(b *Block) *ProofOfWork {
	target := big.NewInt(1)
	target.Lsh(target, uint(256-Difficulty))

	pow := &ProofOfWork{b, target}

	return pow
}

func ToHex(num int64) ([]byte, error) {
	buff := new(bytes.Buffer)
	err := binary.Write(buff, binary.BigEndian, num)
	if err != nil {
		return nil, err
	}
	return buff.Bytes(), nil
}

func (pow *ProofOfWork) InitNonce(nonce int) ([]byte, error) {
	nonceHex, err := ToHex(int64(nonce))
	if err != nil {
		return nil, err
	}
	difficultyHex, err := ToHex(int64(Difficulty))
	if err != nil {
		return nil, err
	}

	data := bytes.Join(
		[][]byte{
			pow.Block.PrevHash,
			pow.Block.Data,
			nonceHex,
			difficultyHex,
		},
		[]byte{},
	)
	return data, nil
}

func (pow *ProofOfWork) Run() (int, []byte) {
	var intHash big.Int
	var hash [32]byte

	nonce := 0
	// This is essentially an infinite loop due to how large
	// MaxInt64 is.
	for nonce < math.MaxInt64 {
		data, err := pow.InitNonce(nonce)
		if err != nil {
			util.HandleFatal(err)
		}
		hash = sha256.Sum256(data)

		fmt.Printf("\r%x", hash)
		intHash.SetBytes(hash[:])

		if intHash.Cmp(pow.Target) == -1 {
			break
		} else {
			nonce++
		}

	}
	fmt.Println()

	return nonce, hash[:]
}

func (pow *ProofOfWork) Validate() bool {
	var intHash big.Int

	data, err := pow.InitNonce(pow.Block.Nonce)
	if err != nil {
		return false
	}

	hash := sha256.Sum256(data)
	intHash.SetBytes(hash[:])

	return intHash.Cmp(pow.Target) == -1
}
