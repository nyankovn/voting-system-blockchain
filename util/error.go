package util

import (
	"fmt"
	"log"
)

const (
	ErrorCodeNotFound     = "not_found"
	ErrorCodeInvalid      = "invalid"
	ErrorCodeInternal     = "internal"
	ErrorCodeUnauthorized = "unauthorized"
	ErrorCodeUnsupported  = "unsupported"

	ErrorStatusNotFound     = 404
	ErrorStatusInvalid      = 400
	ErrorStatusInternal     = 500
	ErrorStatusUnauthorized = 403
	ErrorStatusUnsupported  = 415
)

type Error struct {
	Status  int
	Code    string
	Message string
}

func (e *Error) Error() string {
	return e.Message
}

func WrapErrorf(origin error, status int, code string, format string, args ...interface{}) error {
	return &Error{
		Status:  status,
		Code:    code,
		Message: fmt.Sprintf(format, args...),
	}
}

func NewErrorf(status int, code string, format string, args ...interface{}) error {
	return WrapErrorf(nil, status, code, format, args...)
}

type Response struct {
	Data    interface{}
	Status  int
	Error   error
	Message string
}

func HandleFatal(err error) {
	if err != nil {
		log.Fatalln(err)
	}
}
