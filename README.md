# Build Blockchain using Golang

## What is blockchain?

A blockchain is a distributed network of nodes with equal rights that keeps the data inside it immutable. Each of these nodes has a copy of the data records. If a new record should be added a new transaction is created and should be approved by all nodes (usually done by a consensus mechanism). All transactions are organized into blocks including a hash that refers each block to the one before it.

## How to ensure that blockchain is immutable and decentralized?

As it is described above, the blocks in the ledger are immutable. Therefore, it is not possible to change them and there are responsible for keeping track of the history of the transactions. Moreover, the ledger is decentralized because of the fact that the nodes have equal rights. Therefore, it is not possible for a single node to add new blocks to the database and/or validate them. However, if a new block needs to be added to the ledger all nodes should agree on that. This is done thanks to the **Proof of Work** (**PoW**) concept. PoW is a cryptographic way for a party to prove to others that a certain amount of a specific computational effort has been expended without any previous knowledge. Afterward, the verifiers can check and confirm this expenditure with minimal effort from their side.

## Which are the constituent parts of a blockchain?

- **Block** - Includes Hash, Data and information for the previous block (Previous Hash)
- **Chain** - Collection/ group of blocks
- **Hashing**
- **Genesis Block** - The first block of the chain (therefore there is no information about the previous block because there is not one).
- **Algorithm** - The work that should be done in the PoW method:
- **Nonce** - A random number that can be used just once in a cryptographic communication.

## What does the PoW method do?

- Take the information from the block.
- Make a nonce.
- Produce a hash of the block’s contents plus the nonce.
- Examine the following hash to determine whether it satisfies the conditions stated above. (The first few bytes must be zeroes.)

## What does the creation of a new block looks like?

- Creating a blank block
- Making a NewProofOfWork
- Running the algorithm on the pow
- Storing the Hash of the new block
- Storing the Nonce of the new block
