package main

import (
	"log"
	"os"

	"gitlab.com/nyankovn/voting-system-blockchain/blockchain"
	"gitlab.com/nyankovn/voting-system-blockchain/cmd"
)

func main() {
	defer os.Exit(0)

	chain, err := blockchain.InitBlockChain()
	if err != nil {
		log.Panic(err)
	}
	defer chain.Database.Close()

	cli := cmd.CommandLine{chain}

	cli.Run()
}
